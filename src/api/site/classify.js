import request from '@/utils/request'

// 查询关于我们分类列表
export function listClassify(query) {
  return request({
    url: '/system/site/about/classify/list',
    method: 'get',
    params: query
  })
}

// 查询关于我们分类详细
export function getClassify(id) {
  return request({
    url: '/system/site/about/classify/' + id,
    method: 'get'
  })
}

// 新增关于我们分类
export function addClassify(data) {
  return request({
    url: '/system/site/about/classify',
    method: 'post',
    data: data
  })
}

// 修改关于我们分类
export function updateClassify(data) {
  return request({
    url: '/system/site/about/classify',
    method: 'put',
    data: data
  })
}

// 删除关于我们分类
export function delClassify(id) {
  return request({
    url: '/system/site/about/classify/' + id,
    method: 'delete'
  })
}

// 导出关于我们分类
export function exportClassify(query) {
  return request({
    url: '/system/site/about/classify/export',
    method: 'get',
    params: query
  })
}