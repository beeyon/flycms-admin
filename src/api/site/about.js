import request from '@/utils/request'

// 查询网站相关介绍列表
export function listAbout(query) {
  return request({
    url: '/system/site/about/list',
    method: 'get',
    params: query
  })
}

// 查询网站相关介绍详细
export function getAbout(id) {
  return request({
    url: '/system/site/about/' + id,
    method: 'get'
  })
}

// 新增网站相关介绍
export function addAbout(data) {
  return request({
    url: '/system/site/about',
    method: 'post',
    data: data
  })
}

// 修改网站相关介绍
export function updateAbout(data) {
  return request({
    url: '/system/site/about',
    method: 'put',
    data: data
  })
}

// 删除网站相关介绍
export function delAbout(id) {
  return request({
    url: '/system/site/about/' + id,
    method: 'delete'
  })
}

// 导出网站相关介绍
export function exportAbout(query) {
  return request({
    url: '/system/site/about/export',
    method: 'get',
    params: query
  })
}