import request from '@/utils/request'

// 查询IP地址库列表
export function listIpAddress(query) {
  return request({
    url: '/system/data/ipAddress/list',
    method: 'get',
    params: query
  })
}

// 查询IP地址库详细
export function getIpAddress(id) {
  return request({
    url: '/system/data/ipAddress/' + id,
    method: 'get'
  })
}

// 新增IP地址库
export function addIpAddress(data) {
  return request({
    url: '/system/data/ipAddress',
    method: 'post',
    data: data
  })
}

// 修改IP地址库
export function updateIpAddress(data) {
  return request({
    url: '/system/data/ipAddress',
    method: 'put',
    data: data
  })
}

// 删除IP地址库
export function delIpAddress(id) {
  return request({
    url: '/system/data/ipAddress/' + id,
    method: 'delete'
  })
}

// 导出IP地址库
export function exportIpAddress(query) {
  return request({
    url: '/system/data/ipAddress/export',
    method: 'get',
    params: query
  })
}