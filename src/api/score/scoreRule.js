import request from '@/utils/request'

// 查询积分规则列表
export function listScoreRule(query) {
  return request({
    url: '/system/score/scoreRule/list',
    method: 'get',
    params: query
  })
}

// 查询积分规则详细
export function getScoreRule(id) {
  return request({
    url: '/system/score/scoreRule/' + id,
    method: 'get'
  })
}

// 新增积分规则
export function addScoreRule(data) {
  return request({
    url: '/system/score/scoreRule',
    method: 'post',
    data: data
  })
}

// 修改积分规则
export function updateScoreRule(data) {
  return request({
    url: '/system/score/scoreRule',
    method: 'put',
    data: data
  })
}

// 积分规则修改
export function changeScoreStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/score/scoreRule/changeStatus',
    method: 'put',
    data: data
  })
}

// 删除积分规则
export function delScoreRule(id) {
  return request({
    url: '/system/score/scoreRule/' + id,
    method: 'delete'
  })
}

// 导出积分规则
export function exportScoreRule(query) {
  return request({
    url: '/system/score/scoreRule/export',
    method: 'get',
    params: query
  })
}
