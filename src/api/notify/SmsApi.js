import request from '@/utils/request'

// 查询短信接口列表
export function listSmsApi(query) {
  return request({
    url: '/system/notify/SmsApi/list',
    method: 'get',
    params: query
  })
}

// 查询短信接口详细
export function getSmsApi(id) {
  return request({
    url: '/system/notify/SmsApi/' + id,
    method: 'get'
  })
}

// 新增短信接口
export function addSmsApi(data) {
  return request({
    url: '/system/notify/SmsApi',
    method: 'post',
    data: data
  })
}

// 修改短信接口
export function updateSmsApi(data) {
  return request({
    url: '/system/notify/SmsApi',
    method: 'put',
    data: data
  })
}

// 删除短信接口
export function delSmsApi(id) {
  return request({
    url: '/system/notify/SmsApi/' + id,
    method: 'delete'
  })
}

// 导出短信接口
export function exportSmsApi(query) {
  return request({
    url: '/system/notify/SmsApi/export',
    method: 'get',
    params: query
  })
}