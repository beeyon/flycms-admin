import request from '@/utils/request'

// 查询系统邮件模板设置列表
export function listTemplet(query) {
  return request({
    url: '/system/notify/templet/list',
    method: 'get',
    params: query
  })
}

// 查询系统邮件模板设置详细
export function getTemplet(id) {
  return request({
    url: '/system/notify/templet/' + id,
    method: 'get'
  })
}

// 新增系统邮件模板设置
export function addTemplet(data) {
  return request({
    url: '/system/notify/templet',
    method: 'post',
    data: data
  })
}

// 修改系统邮件模板设置
export function updateTemplet(data) {
  return request({
    url: '/system/notify/templet',
    method: 'put',
    data: data
  })
}

// 删除系统邮件模板设置
export function delTemplet(id) {
  return request({
    url: '/system/notify/templet/' + id,
    method: 'delete'
  })
}

// 导出系统邮件模板设置
export function exportTemplet(query) {
  return request({
    url: '/system/notify/templet/export',
    method: 'get',
    params: query
  })
}