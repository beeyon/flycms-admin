import request from '@/utils/request'

// 查询短信验证码列表
export function listSmsCode(query) {
  return request({
    url: '/system/notify/SmsCode/list',
    method: 'get',
    params: query
  })
}

// 查询短信验证码详细
export function getSmsCode(id) {
  return request({
    url: '/system/notify/SmsCode/' + id,
    method: 'get'
  })
}

// 新增短信验证码
export function addSmsCode(data) {
  return request({
    url: '/system/notify/SmsCode',
    method: 'post',
    data: data
  })
}

// 修改短信验证码
export function updateSmsCode(data) {
  return request({
    url: '/system/notify/SmsCode',
    method: 'put',
    data: data
  })
}

// 删除短信验证码
export function delSmsCode(id) {
  return request({
    url: '/system/notify/SmsCode/' + id,
    method: 'delete'
  })
}

// 导出短信验证码
export function exportSmsCode(query) {
  return request({
    url: '/system/notify/SmsCode/export',
    method: 'get',
    params: query
  })
}