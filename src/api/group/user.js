import request from '@/utils/request'

// 查询群组和用户对应关系列表
export function listUser(query) {
  return request({
    url: '/system/group/user/list',
    method: 'get',
    params: query
  })
}

// 查询群组和用户对应关系详细
export function getUser(userId) {
  return request({
    url: '/system/group/user/' + userId,
    method: 'get'
  })
}

// 新增群组和用户对应关系
export function addUser(data) {
  return request({
    url: '/system/group/user',
    method: 'post',
    data: data
  })
}

// 修改群组和用户对应关系
export function updateUser(data) {
  return request({
    url: '/system/group/user',
    method: 'put',
    data: data
  })
}

// 删除群组和用户对应关系
export function delUser(userId) {
  return request({
    url: '/system/group/user/' + userId,
    method: 'delete'
  })
}

// 导出群组和用户对应关系
export function exportUser(query) {
  return request({
    url: '/system/group/user/export',
    method: 'get',
    params: query
  })
}