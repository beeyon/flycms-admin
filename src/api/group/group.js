import request from '@/utils/request'

// 查询群组(小组)列表
export function listGroup(query) {
  return request({
    url: '/system/group/group/list',
    method: 'get',
    params: query
  })
}

// 查询群组(小组)详细
export function getGroup(id) {
  return request({
    url: '/system/group/group/' + id,
    method: 'get'
  })
}

// 新增群组(小组)
export function addGroup(data) {
  return request({
    url: '/system/group/group',
    method: 'post',
    data: data
  })
}

// 修改群组(小组)
export function updateGroup(data) {
  return request({
    url: '/system/group/group',
    method: 'put',
    data: data
  })
}

// 删除群组(小组)
export function delGroup(id) {
  return request({
    url: '/system/group/group/' + id,
    method: 'delete'
  })
}

// 导出群组(小组)
export function exportGroup(query) {
  return request({
    url: '/system/group/group/export',
    method: 'get',
    params: query
  })
}