import request from '@/utils/request'

// 查询小组分类列表
export function listColumn(query) {
  return request({
    url: '/system/group/column/list',
    method: 'get',
    params: query
  })
}

// 查询小组分类详细
export function getColumn(id) {
  return request({
    url: '/system/group/column/' + id,
    method: 'get'
  })
}

// 新增小组分类
export function addColumn(data) {
  return request({
    url: '/system/group/column',
    method: 'post',
    data: data
  })
}

// 修改小组分类
export function updateColumn(data) {
  return request({
    url: '/system/group/column',
    method: 'put',
    data: data
  })
}

// 删除小组分类
export function delColumn(id) {
  return request({
    url: '/system/group/column/' + id,
    method: 'delete'
  })
}

// 导出小组分类
export function exportColumn(query) {
  return request({
    url: '/system/group/column/export',
    method: 'get',
    params: query
  })
}