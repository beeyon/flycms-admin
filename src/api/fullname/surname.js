import request from '@/utils/request'

// 查询姓氏列表
export function listSurname(query) {
  return request({
    url: '/system/fullname/surname/list',
    method: 'get',
    params: query
  })
}

// 查询姓氏详细
export function getSurname(id) {
  return request({
    url: '/system/fullname/surname/' + id,
    method: 'get'
  })
}

// 新增姓氏
export function addSurname(data) {
  return request({
    url: '/system/fullname/surname',
    method: 'post',
    data: data
  })
}

// 修改姓氏
export function updateSurname(data) {
  return request({
    url: '/system/fullname/surname',
    method: 'put',
    data: data
  })
}

// 删除姓氏
export function delSurname(id) {
  return request({
    url: '/system/fullname/surname/' + id,
    method: 'delete'
  })
}

// 导出姓氏
export function exportSurname(query) {
  return request({
    url: '/system/fullname/surname/export',
    method: 'get',
    params: query
  })
}