import request from '@/utils/request'

// 查询姓名搜索列表
export function listKeyword(query) {
  return request({
    url: '/system/fullname/keyword/list',
    method: 'get',
    params: query
  })
}

// 查询姓名搜索详细
export function getKeyword(id) {
  return request({
    url: '/system/fullname/keyword/' + id,
    method: 'get'
  })
}

// 新增姓名搜索
export function addKeyword(data) {
  return request({
    url: '/system/fullname/keyword',
    method: 'post',
    data: data
  })
}

// 修改姓名搜索
export function updateKeyword(data) {
  return request({
    url: '/system/fullname/keyword',
    method: 'put',
    data: data
  })
}

// 删除姓名搜索
export function delKeyword(id) {
  return request({
    url: '/system/fullname/keyword/' + id,
    method: 'delete'
  })
}

// 导出姓名搜索
export function exportKeyword(query) {
  return request({
    url: '/system/fullname/keyword/export',
    method: 'get',
    params: query
  })
}